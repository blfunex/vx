import resolve from "@rollup/plugin-node-resolve";
import ts from "@rollup/plugin-typescript";
import sucrase from "@rollup/plugin-sucrase";
import { terser } from "rollup-plugin-terser";
import replace from "@rollup/plugin-replace";

const production = process.env.BUNDLE_ENV == "production";
const unstable = process.env.BUNDLE_ENV == "unstable";

const development = unstable || !production;
const pipeline = unstable || production;

setTimeout(() => process.exit(0), 15000);

export default [
  {
    external: [],
    input: "./src/app/main.ts",
    output: {
      file: `./public/${
        pipeline ? (production ? "main" : "unstable") : "main"
      }/bundle.min.js`,
      sourcemap: true,
      format: "esm",
    },
    plugins: [
      resolve(),
      replace({
        values: {
          '"/assets/': pipeline ? '"/vx/assets/' : '"../assets/',
          "process.env.NODE_ENV": JSON.stringify(
            development ? "development" : "production",
          ),
        },
        preventAssignment: true,
      }),
      production ? ts() : sucrase({ transforms: ["typescript"] }),
      pipeline &&
        terser({
          ecma: 2020,
          compress: true,
          mangle: {
            keep_fnames: false,
            keep_classnames: false,
            properties: {
              keep_quoted: true,
              regex: /^_|^\$?[A-Z]/,
            },
          },
        }),
    ],
  },
];
