import { gl } from "./canvas";

export default abstract class Texture implements TextureParameters {
  private readonly anisotropicExt = gl.getExtension("EXT_texture_filter_anisotropic");

  constructor(
    public target: number,
    {
      mips = false,
      min = gl.NEAREST,
      mag = gl.NEAREST,
      flipY = true,
      wrap = target == gl.TEXTURE_CUBE_MAP ? gl.CLAMP_TO_EDGE : gl.REPEAT,
      format = gl.RGBA,
      encoding = getEncoding(format),
      anisotropy = 1,
    }: Partial<TextureParameters> = {},
  ) {
    this.handle = gl.createTexture()!;
    this.setParameters({
      min,
      mag,
      mips,
      wrap,
      flipY,
      format,
      encoding,
      anisotropy,
    });
  }

  setParameters({
    min = this.min,
    mag = this.mag,
    wrap = this.wrap,
    anisotropy = this.anisotropy,
    format = this.format,
    encoding = this.format == format ? this.encoding : getEncoding(format),
    mips = this.mips,
    flipY = this.flipY,
  }: Partial<TextureParameters> = {}) {
    const target = this.target;

    this.bind();

    if (min != this.min) gl.texParameteri(target, gl.TEXTURE_MIN_FILTER, min);

    if (mag != this.mag) gl.texParameteri(target, gl.TEXTURE_MAG_FILTER, mag);

    if (wrap != this.wrap) {
      if (Array.isArray(wrap)) {
        gl.texParameteri(target, gl.TEXTURE_WRAP_S, wrap[0]);
        gl.texParameteri(target, gl.TEXTURE_WRAP_T, wrap[1]);
        if (wrap.length > 2) gl.texParameteri(target, gl.TEXTURE_WRAP_R, wrap[2]!);
      } else {
        gl.texParameteri(target, gl.TEXTURE_WRAP_S, wrap);
        gl.texParameteri(target, gl.TEXTURE_WRAP_T, wrap);
        switch (target) {
          case gl.TEXTURE_2D_ARRAY:
          case gl.TEXTURE_3D:
            gl.texParameteri(target, gl.TEXTURE_WRAP_R, wrap);
        }
      }
    }

    if (anisotropy != this.anisotropy) {
      const anisotropicExt = this.anisotropicExt;
      if (anisotropicExt) {
        const max = gl.getParameter(anisotropicExt.MAX_TEXTURE_MAX_ANISOTROPY_EXT);
        gl.texParameterf(target, anisotropicExt.TEXTURE_MAX_ANISOTROPY_EXT, Math.min(anisotropy, max));
      }
    }

    this.min = min;
    this.mag = mag;
    this.wrap = wrap;
    this.encoding = encoding;
    this.format = format;
    this.anisotropy = anisotropy;
    this.mips = mips;
    this.flipY = flipY;
  }

  bind() {
    gl.bindTexture(this.target, this.handle);
  }

  delete() {
    gl.deleteTexture(this.handle);
  }

  protected setImageSource2D(image: TexImageSource) {
    gl.texImage2D(gl.TEXTURE_2D, 0, this.encoding, this.format, gl.UNSIGNED_BYTE, image);
  }

  protected setImageView2D(image: ArrayBufferView | null, width: number, height: number, type = gl.UNSIGNED_BYTE) {
    gl.texImage2D(gl.TEXTURE_2D, 0, this.encoding, width, height, 0, this.format, type, image);
  }

  protected onBeforeUpload() {
    if (this.flipY) gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
  }

  protected onAfterUpload() {
    if (this.mips) gl.generateMipmap(this.target);
  }

  protected setImageSource3D(
    image: TexImageSource | ArrayBufferView,
    width: number,
    height: number,
    depth: number,
    type = gl.UNSIGNED_BYTE,
  ) {
    gl.texImage3D(
      gl.TEXTURE_3D,
      0,
      this.encoding,
      width,
      height,
      depth,
      0,
      this.format,
      type,
      image as ArrayBufferView,
    );
  }

  protected setImageSourceCube(image: TexImageSource, face: number) {
    gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + face, 0, this.encoding, this.format, gl.UNSIGNED_BYTE, image);
  }

  protected setImageViewCube(
    image: ArrayBufferView,
    face: number,
    width: number,
    height: number,
    type = gl.UNSIGNED_BYTE,
  ) {
    gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + face, 0, this.encoding, width, height, 0, this.format, type, image);
  }

  protected setImageSourceList(
    image: TexImageSource | ArrayBufferView,
    width: number,
    height: number,
    count: number,
    type = gl.UNSIGNED_BYTE,
  ) {
    gl.texImage3D(
      gl.TEXTURE_2D_ARRAY,
      0,
      this.encoding,
      width,
      height,
      count,
      0,
      this.format,
      type,
      image as ArrayBufferView,
    );
  }

  handle: WebGLTexture;
  min!: number;
  mag!: number;
  wrap!: number | [number, number] | [number, number, number];
  encoding!: number;
  format!: number;
  anisotropy!: number;
  mips!: boolean;
  flipY!: boolean;
}

export interface TextureParameters {
  min: number;
  mag: number;
  wrap: number | [number, number] | [number, number, number];
  format: number;
  encoding: number;
  anisotropy: number;
  mips: boolean;
  flipY: boolean;
}

function getEncoding(format: number) {
  switch (format) {
    case gl.LUMINANCE:
      return gl.LUMINANCE;
    case gl.ALPHA:
      return gl.ALPHA;
    case gl.LUMINANCE_ALPHA:
      return gl.LUMINANCE_ALPHA;
    case gl.RGB:
      return gl.RGB;
    case gl.RGBA:
      return gl.RGBA;
    case gl.DEPTH_COMPONENT:
      return gl.DEPTH_COMPONENT;
    case gl.DEPTH_STENCIL:
      return gl.DEPTH_STENCIL;
    case gl.RED:
      return gl.RED;
    case gl.RG:
      return gl.RG;
    case gl.RGB_INTEGER:
      return gl.RGB_INTEGER;
    case gl.RGBA_INTEGER:
      return gl.RGBA_INTEGER;
    case gl.DEPTH_COMPONENT16:
      return gl.DEPTH_COMPONENT16;
    case gl.DEPTH_STENCIL:
      return gl.DEPTH_STENCIL;
    default:
      throw new Error(`Unsupported texture format: ${format}`);
  }
}
