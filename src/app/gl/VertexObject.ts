import ElementBuffer from "./buffers/ElementBuffer";
import VertexBuffer, { VertexUnit } from "./buffers/VertexBuffer";
import { gl } from "./canvas";

export default class VertexObject {
  private _handle: WebGLVertexArrayObject | null;

  constructor() {
    this._handle = gl.createVertexArray();
  }

  bind() {
    gl.bindVertexArray(this._handle);
  }

  delete() {
    gl.deleteVertexArray(this._handle);
  }

  private _vertices: VertexBuffer[] = [];
  private _units: [start: number, units: VertexUnit[]][] = [];
  private _start = 0;

  add(...units: VertexUnit[]) {
    this._units.push([this._start, units]);
    this._vertices.push(new VertexBuffer());
    this._start += units.length;
    return this;
  }

  get vertices(): readonly Omit<
    VertexBuffer,
    "draw" | "defineAttributes" | "defineAttribute"
  >[] {
    return this._vertices;
  }

  readonly elements = new ElementBuffer();

  define() {
    this.bind();
    this.elements.bind();
    const count = this._vertices.length;
    for (let i = 0; i < count; ++i) {
      const buffer = this._vertices[i];
      const [start, units] = this._units[i];
      buffer.bind();
      buffer.defineAttributes(start, ...units);
    }
  }

  draw(mode: number) {
    this.elements.draw(mode);
  }
}
