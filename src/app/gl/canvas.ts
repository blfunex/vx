import { error, ErrorCode } from "../utils/error";

export const canvas = (_ => {
  let canvas = document.querySelector("canvas");

  if (canvas) return canvas;

  canvas = document.createElement("canvas");

  canvas.height = 1080;
  canvas.width = 1920;

  document.body.appendChild(canvas);

  return canvas;
})();

const context = canvas.getContext("webgl2", { alpha: false, depth: true });

if (!context) error(ErrorCode.CREATE_GL_CONTEXT_FAIL);

const debug = false;
export const gl = (debug ? addDebug : id)(context);

function id<T>(x: T) {
  return x;
}

function addDebug(context: WebGL2RenderingContext) {
  const gl: WebGL2RenderingContext = Object.create(context);

  const prototype = Object.getPrototypeOf(context);

  for (const name in context) {
    // @ts-ignore
    const fn = context[name];
    if (typeof fn != "function") {
      const descriptor = Object.getOwnPropertyDescriptor(prototype, name)!;
      if (!descriptor.value)
        Object.defineProperty(gl, name, {
          // @ts-ignore
          get: () => context[name],
          set(it) {
            // @ts-ignore
            context[name] = it;
          },
        });
      continue;
    }

    // @ts-ignore
    gl[name] = Function(
      `(function ${name}() {
    const result = fn.apply(context, arguments);
    const error = context.getError();
    if (error != context.NO_ERROR)
      throw new Error(\`\${getErrorName(error)} 0x\${error.toString(16)} (${name})\`);
    return result;
  })`,
      "fn",
      "getErrorName",
    )(fn, getErrorName);
  }

  return gl;

  function getErrorName(error: number) {
    switch (error) {
      case context.INVALID_ENUM:
        return "INVALID_ENUM";
      case context.INVALID_VALUE:
        return "INVALID_VALUE";
      case context.INVALID_OPERATION:
        return "INVALID_OPERATION";
      case context.OUT_OF_MEMORY:
        return "OUT_OF_MEMORY";
      case context.INVALID_FRAMEBUFFER_OPERATION:
        return "INVALID_FRAMEBUFFER_OPERATION";
      case context.CONTEXT_LOST_WEBGL:
        return "CONTEXT_LOST_WEBGL";
      default:
        return "UNKNOWN";
    }
  }
}
