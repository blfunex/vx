import {
  ReadonlyMat2,
  ReadonlyMat3,
  ReadonlyMat4,
  ReadonlyVec2,
  ReadonlyVec3,
  ReadonlyVec4,
} from "gl-matrix";
import { gl } from "./canvas";
import Texture from "./Texture";

export default class Shader {
  handle: WebGLProgram;
  uniforms: Record<string, UniformMetadata>;

  constructor(vert: string, frag: string) {
    const uniforms: Record<string, UniformMetadata> = Object.create(null);
    this.handle = linkProgram(vert, frag, uniforms);
    this.uniforms = uniforms;
  }

  use() {
    gl.useProgram(this.handle);
  }

  delete() {
    gl.deleteProgram(this.handle);
  }

  setFloat(name: string, value: number) {
    const uniform = this.uniforms[name];
    if (!uniform) return;
    gl.uniform1f(uniform.location, value);
  }

  setInt(name: string, value: number) {
    const uniform = this.uniforms[name];
    if (!uniform) return;
    gl.uniform1i(uniform.location, value);
  }

  setFloat2(name: string, x: number, y: number) {
    const uniform = this.uniforms[name];
    if (!uniform) return;
    gl.uniform2f(uniform.location, x, y);
  }

  setFloat3(name: string, x: number, y: number, z: number) {
    const uniform = this.uniforms[name];
    if (!uniform) return;
    gl.uniform3f(uniform.location, x, y, z);
  }

  setFloat4(name: string, x: number, y: number, z: number, w: number) {
    const uniform = this.uniforms[name];
    if (!uniform) return;
    gl.uniform4f(uniform.location, x, y, z, w);
  }

  setVector1(name: string, vector: readonly [number] | Float32Array) {
    const uniform = this.uniforms[name];
    if (!uniform) return;
    gl.uniform1fv(uniform.location, vector);
  }

  setVector2(name: string, vector: ReadonlyVec2) {
    const uniform = this.uniforms[name];
    if (!uniform) return;
    gl.uniform2fv(uniform.location, vector);
  }

  setVector3(name: string, vector: ReadonlyVec3) {
    const uniform = this.uniforms[name];
    if (!uniform) return;
    gl.uniform3fv(uniform.location, vector);
  }

  setVector4(name: string, vector: ReadonlyVec4) {
    const uniform = this.uniforms[name];
    if (!uniform) return;
    gl.uniform4fv(uniform.location, vector);
  }

  setMatrix2(name: string, matrix: ReadonlyMat2) {
    const uniform = this.uniforms[name];
    if (!uniform) return;
    gl.uniformMatrix2fv(uniform.location, false, matrix);
  }

  setMatrix3(name: string, matrix: ReadonlyMat3) {
    const uniform = this.uniforms[name];
    if (!uniform) return;
    gl.uniformMatrix3fv(uniform.location, false, matrix);
  }

  setMatrix4(name: string, matrix: ReadonlyMat4) {
    const uniform = this.uniforms[name];
    if (!uniform) return;
    gl.uniformMatrix4fv(uniform.location, false, matrix);
  }

  setTexture(name: string, texture: Texture, unit = 0) {
    gl.activeTexture(gl.TEXTURE0 + unit);
    gl.bindTexture(texture.target, texture.handle);
    this.setInt(name, unit);
  }
}

class UniformMetadata {
  constructor(
    readonly location: WebGLUniformLocation,
    readonly name: string,
    readonly size: number,
    readonly type: number,
  ) {}
}

function compileShader(source: string, type: number) {
  const shader = gl.createShader(type)!;
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    const log = gl.getShaderInfoLog(shader)!;
    gl.deleteShader(shader);
    throw new SyntaxError(`Failed to compile shader: ${log}`);
  }
  return shader;
}

function linkProgram(
  vert: string,
  frag: string,
  uniforms: Record<string, UniformMetadata>,
) {
  const program = gl.createProgram()!;

  let v!: WebGLShader, f!: WebGLShader;

  try {
    v = compileShader(vert, gl.VERTEX_SHADER);
    f = compileShader(frag, gl.FRAGMENT_SHADER);

    gl.attachShader(program, v);
    gl.attachShader(program, f);

    gl.linkProgram(program);

    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
      const log = gl.getProgramInfoLog(program)!;
      gl.deleteProgram(program);
      throw new Error(`Failed to link shader program: ${log}`);
    }

    const count: number = gl.getProgramParameter(program, gl.ACTIVE_UNIFORMS);
    for (let i = 0; i < count; i++) {
      const { name, size, type } = gl.getActiveUniform(program, i)!;
      const location = gl.getUniformLocation(program, name)!;

      const metadata = new UniformMetadata(location, name, size, type);

      uniforms[name] = metadata;
    }

    gl.detachShader(program, v);
    gl.detachShader(program, f);

    return program;
  } finally {
    gl.deleteShader(v);
    gl.deleteShader(f);
  }
}
