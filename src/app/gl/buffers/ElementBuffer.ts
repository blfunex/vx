import { gl } from "../canvas";
import Buffer from "./Buffer";

export default class ElementBuffer extends Buffer<Uint16Array | Uint8Array> {
  constructor() {
    super(gl.ELEMENT_ARRAY_BUFFER);
  }

  private _type = gl.UNSIGNED_SHORT;
  private _count = 0;

  protected __normalizeData(data: Uint16Array | Uint8Array | number[]) {
    const value = ArrayBuffer.isView(data) ? data : new Uint16Array(data);
    const type = value.constructor.name;
    this._type = type == "Uint16Array" ? gl.UNSIGNED_SHORT : gl.UNSIGNED_BYTE;
    this._count = value.length;
    return value;
  }

  draw(mode: number, count = this._count, offset = 0) {
    gl.drawElements(mode, count, this._type, offset);
  }
}
