import { gl } from "../canvas";

export default abstract class Buffer<T extends ArrayBufferView> {
  private _handle: WebGLBuffer;
  private _target: number;

  constructor(target: number) {
    this._target = target;
    this._handle = gl.createBuffer()!;
  }

  bind() {
    gl.bindBuffer(this._target, this._handle);
  }

  delete() {
    gl.deleteBuffer(this._handle);
  }

  setData(data: T | number[], usage = gl.DYNAMIC_DRAW) {
    gl.bufferData(this._target, this.__normalizeData(data), usage);
  }

  protected abstract __normalizeData(data: T | number[]): T;
}
