import { gl } from "../canvas";
import Buffer from "./Buffer";

export type VertexUnit = 1 | 2 | 3 | 4 | 9 | 16;

export default class VertexBuffer extends Buffer<Float32Array> {
  constructor() {
    super(gl.ARRAY_BUFFER);
  }

  private _count = 0;

  protected __normalizeData(data: Float32Array | number[]) {
    if (this._stride == 0)
      throw new Error("Stride is not defined, please add an attribute");
    const value = ArrayBuffer.isView(data) ? data : new Float32Array(data);
    this._count = value.length / this._stride;
    return value;
  }

  draw(mode: number, count = this._count, offset = 0) {
    gl.drawArrays(mode, offset, count);
  }

  private _stride = 0;

  defineAttribute(index: number, unit: VertexUnit, stride = 0, offset = 0) {
    this._stride = stride == 0 ? unit : stride;
    this._defineAttribute(index, unit, stride, offset);
  }

  private _defineAttribute(index: number, unit: number, stride: number, offset: number) {
    gl.enableVertexAttribArray(index);
    gl.vertexAttribPointer(index, unit, gl.FLOAT, false, stride * 4, offset * 4);
  }

  defineAttributes(start: number, ...units: VertexUnit[]) {
    const stride = (this._stride = units.reduce(add, 0));
    units.reduce((acc, unit, index) => {
      this._defineAttribute(index + start, unit, stride, acc);
      return acc + unit;
    }, 0);
  }
}

function add(a: number, b: number) {
  return a + b;
}
