import { gl } from "./canvas";
import Texture, { TextureParameters } from "./Texture";

export default class ImageTexture extends Texture {
  constructor(parameters?: Partial<TextureParameters>) {
    super(gl.TEXTURE_2D, parameters);
  }

  private _image: TexImageSource | null = null;

  set image(image) {
    const current = this._image!;
    if (image == current) return;
    this.bind();
    this.onBeforeUpload();
    if (image) super.setImageSource2D(image);
    else super.setImageView2D(null, current.width, current.height);
    this.onAfterUpload();
    this._image = image;
  }

  get image() {
    return this._image;
  }

  delete() {
    super.delete();
    this._image = null;
  }
}
