import type TileController from "./TileController";
import type { RegistredTileController } from "./TileController";

import Chunk, { checkIsOutsideWorld } from "./Chunk";
import ChunkMesher from "../mesher/ChunkMesher";
import { createTileControllerList } from "./TileController";

export default class World {
  private mesher = new ChunkMesher();

  generateMeshes() {
    const loaded = this.loaded;
    const mesher = this.mesher;
    for (const coords in loaded) {
      mesher.generate(loaded[coords]);
    }
  }

  geTileController(id: number) {
    if (id < this.tiles.length) return this.tiles[id];
    throw new Error(`Unknown tile with id ${id}`);
  }

  private loaded: Record<string, Chunk> = Object.create(null);
  private tiles = createTileControllerList();

  get chunks() {
    return Object.values(this.loaded);
  }

  addTileController(name: string, definition: TileController) {
    const tile = definition as RegistredTileController;
    const tiles = this.tiles;
    const id = tiles.length;
    tiles[id] = tile;
    tile.id = id;
    tile.name = name;
    return tile;
  }

  setTile(x: number, y: number, z: number, tile: RegistredTileController, data?: number) {
    if (checkIsOutsideWorld(y)) throw new Error("Can not set tile outside world.");
    this.getChunkOrNew(x >> 4, z >> 4).setTileUnchecked(x & 15, y, z & 15, tile, data);
  }

  getChunkOrNew(cx: number, cy: number) {
    const chunk = this.getChunkMaybe(cx, cy);

    if (chunk) return chunk;

    const instance = new Chunk(this, cx, cy);

    this.loaded[instance.coords] = instance;

    return instance;
  }

  getChunkMaybe(cx: number, cy: number) {
    const coords = Chunk.getChunkCoordString(cx, cy);

    const chunks = this.loaded;

    if (Reflect.has(chunks, coords)) return chunks[coords];

    return null;
  }
}
