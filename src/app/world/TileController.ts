import type ChunkMesher from "../mesher/ChunkMesher";

import { TileSide, TileSideMask } from "../mesher/TileSide";

type TileController = Readonly<{
  getOccludingMask(data: number): TileSideMask;
  usesOcclusionMask: boolean;
  contributesToChunkHeight: boolean;
  generate(
    generator: ChunkMesher,
    x: number,
    y: number,
    z: number,
    side: TileSide | -1,
    data: number,
  ): void;
}>;

export default TileController;

export type RegistredTileController = TileController & { id: number; name: string };

export const enum ReservedTileIDs {
  AIR,
  VOID,
}

const listTemplate: RegistredTileController[] = [];

export function createTileControllerList() {
  return Array.from(listTemplate);
}

listTemplate[ReservedTileIDs.VOID] = {
  id: ReservedTileIDs.VOID,
  name: "vx:void",
  usesOcclusionMask: false,
  contributesToChunkHeight: false,
  generate() {},
  getOccludingMask() {
    return TileSideMask.SIDES;
  },
};

listTemplate[ReservedTileIDs.AIR] = {
  id: ReservedTileIDs.AIR,
  name: "vx:air",
  usesOcclusionMask: false,
  contributesToChunkHeight: false,
  generate() {},
  getOccludingMask() {
    return TileSideMask.NONE;
  },
};
