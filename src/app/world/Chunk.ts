import type { RegistredTileController } from "./TileController";
import type World from "./World";

import Mesh from "../mesher/Mesh";
import { ReservedTileIDs } from "./TileController";
import { QuadMesher } from "../mesher/QuadMesher";
import ChunkMesher from "../mesher/ChunkMesher";

export const enum ChunkBitwise {
  U32_MASK = 0xffffffff,
  U12_MASK = 0x00000fff,
  U8_MASK = 0x000000ff,
  U6_MASK = 0x0000003f,
  U4_MASK = 0x0000000f,

  ID_SIZE = 12,
  LIGHT_SIZE = 4,

  ID_OFFSET = 0,
  LIGHT_OFFSET = ID_OFFSET + ID_SIZE,

  ID_MASK = U12_MASK << ID_OFFSET,

  LIGHT_MASK = U4_MASK << LIGHT_OFFSET,
}

const enum step {
  INITIALIZING,
}

export const enum ChunkSizes {
  LENGTH = 16,
  HEIGHT = 256,
  LEVEL = LENGTH ** 2,
  VOLUME = LEVEL * HEIGHT,
}

export function checkIsOutsideWorld(y: number) {
  return y < 0 || y >= ChunkSizes.HEIGHT;
}

export function checkIsOutsideChunk(x: number, z: number) {
  return x < 0 || z < 0 || x >= ChunkSizes.LENGTH || z >= ChunkSizes.LENGTH;
}

function getDataIndex(x: number, y: number, z: number) {
  return x + z * ChunkSizes.LENGTH + y * ChunkSizes.LEVEL;
}

export default class Chunk {
  readonly mesh = new Mesh();

  height = 0;

  private data = new Uint32Array(ChunkSizes.VOLUME);

  private step = step.INITIALIZING;

  constructor(readonly world: World, public cx: number, public cy: number) {}

  private last = 0;

  get lastData() {
    return this.last;
  }

  getDataUnchecked(x: number, y: number, z: number) {
    return (this.last = this.data[getDataIndex(x, y, z)]);
  }

  getDataSync(x: number, y: number, z: number, unknown = ReservedTileIDs.VOID): number {
    if (checkIsOutsideWorld(y)) {
      return unknown;
    } else if (checkIsOutsideChunk(x, z)) {
      const chunk = this.world.getChunkMaybe(
        this.cx + Math.sign(x),
        this.cy + Math.sign(z),
      );
      return (this.last = chunk ? chunk.getDataSync(x, y, z, unknown) : unknown);
    } else {
      return this.getDataUnchecked(x, y, z);
    }
  }

  setTileUnchecked(
    x: number,
    y: number,
    z: number,
    tile: RegistredTileController,
    data = 0,
  ) {
    if (tile.contributesToChunkHeight) this.height = Math.max(this.height, y + 1);
    this.data[getDataIndex(x, y, z)] =
      (data << ChunkBitwise.ID_OFFSET) | (tile.id & ChunkBitwise.ID_MASK);
  }

  getTile(x: number, y: number, z: number, unknown = ReservedTileIDs.AIR) {
    const id = this.getDataSync(x, y, z, unknown);
    return this.world.geTileController(id);
  }

  getOccludingMask(x: number, y: number, z: number) {
    const tile = this.getTile(x, y, z, ReservedTileIDs.VOID);
    const occluding = tile.getOccludingMask(this.last);
    return occluding;
  }

  get coords() {
    return Chunk.getChunkCoordString(this.cx, this.cy);
  }

  static getBlockChunkCoordString(x: number, z: number) {
    return this.getChunkCoordString(x >> 4, z >> 4);
  }

  static getChunkCoordString(cx: number, cy: number) {
    return `[${cx},${cy}]`;
  }
}
