import { ReadonlyVec2, vec2 } from "gl-matrix";
import { canvas } from "../gl/canvas";

export const enum MouseButtonState {
  UP,
  DOWN,
  DRAGGING,
}

type MutableMouseButtons = {
  length: number;
} & {
  [K in number]?: MouseButtonState;
};

export type MouseButtons = Readonly<MutableMouseButtons>;

export class Mouse {
  private _nextButtons: MutableMouseButtons = [];
  private _buttons: MutableMouseButtons = [];
  private _nextPosition: vec2 = [0, 0];
  private _position: vec2 = [0, 0];
  private _nextMovement: vec2 = [0, 0];
  private _movement: vec2 = [0, 0];
  private _handler: (e: MouseEvent) => void;
  private _locking = false;

  constructor() {
    this._handler = e => {
      e.preventDefault();

      const buttons = this._nextButtons;
      const position = this._nextPosition;
      const movement = this._nextMovement;

      if (this._locking && !this.locked && LOCK_MOUSE_EVENT_NAMES.has(e.type)) {
        canvas.requestPointerLock();
        this._locking = false;
      }

      switch (e.type) {
        case "pointerdown":
          buttons[e.button] = MouseButtonState.DOWN;
          return;

        case "pointerup":
          buttons[e.button] = MouseButtonState.UP;
          return;

        case "pointermove": {
          buttons[e.button] =
            buttons[e.button] == MouseButtonState.DOWN
              ? MouseButtonState.DRAGGING
              : MouseButtonState.UP;

          vec2.set(position, e.clientX, e.clientY);

          movement[0] += e.movementX;
          movement[1] += e.movementY;
        }
      }
    };

    this._handle(canvas.addEventListener);
  }

  dispose() {
    this._handle(canvas.removeEventListener);
  }

  private _handle(method: Function) {
    const handler = this._handler;
    for (const name of MOUSE_EVENT_NAMES) {
      method.call(canvas, name, handler);
    }
  }

  update() {
    vec2.copy(this._position, this._nextPosition);
    vec2.copy(this._movement, this._nextMovement);
    vec2.set(this._nextMovement, 0, 0);
    this._buttons = this._nextButtons;
    this._nextButtons = [];
  }

  lock() {
    this._locking = true;
  }

  unlock() {
    if (this.locked) document.exitPointerLock();
    this._locking = false;
  }

  get buttons(): MouseButtons {
    return this._buttons;
  }

  get position(): ReadonlyVec2 {
    return this._position;
  }

  get movement(): ReadonlyVec2 {
    return this._movement;
  }

  get locked() {
    return canvas == document.pointerLockElement;
  }

  get locking() {
    return this._locking;
  }

  get x() {
    return this._position[0];
  }

  get y() {
    return this._position[1];
  }

  get dx() {
    return this._movement[0];
  }

  get dy() {
    return this._movement[1];
  }
}

const LOCK_MOUSE_EVENT_NAMES = new Set([
  "pointerdown",
  "dblclick",
  "click",
  "contextmenu",
  "auxclick",
]);

const MOUSE_EVENT_NAMES = [
  ...LOCK_MOUSE_EVENT_NAMES,
  "pointerup",
  "pointermove",
] as const;
