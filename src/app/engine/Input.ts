import { Keyboard } from "./Keyboard";
import { Mouse } from "./Mouse";

export default class Input {
  readonly mouse = new Mouse();
  readonly keyboard = new Keyboard();

  update() {
    this.mouse.update();
    this.keyboard.update();
  }
}
