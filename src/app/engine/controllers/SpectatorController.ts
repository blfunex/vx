import { vec3 } from "gl-matrix";
import type Camera from "../Camera";
import type Input from "../Input";

const movement = vec3.create();

export default class SpectatorController {
  constructor(readonly camera: Camera, readonly input: Input) {}

  update() {
    const { input, camera } = this;
    const { mouse, keyboard } = input;

    if (mouse.locked) {
      const ms = 0.01;
      const ks = 0.3;

      camera.rotate(mouse.dy * ms, mouse.dx * ms);

      vec3.set(movement, 0, 0, 0);

      if (keyboard.keys["W"]) movement[2] -= 1;
      if (keyboard.keys["S"]) movement[2] += 1;
      if (keyboard.keys["A"]) movement[0] -= 1;
      if (keyboard.keys["D"]) movement[0] += 1;
      if (keyboard.keys["SHIFT"]) movement[1] -= 1;
      if (keyboard.keys["SPACE"]) movement[1] += 1;

      vec3.normalize(movement, movement);
      vec3.scale(movement, movement, ks);

      camera.move(movement[0], movement[1], movement[2]);
    } else {
      mouse.lock();
    }
  }
}
