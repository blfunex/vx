import { mat4 } from "gl-matrix";
import { constant } from "../math/utils";
import Camera from "./Camera";

export default class PerspectiveCamera extends Camera {
  private _aspect = 1;
  private _fov = 45;
  private _near = 0.1;
  private _far = 1000;

  get aspect() {
    return this._aspect;
  }

  set aspect(value) {
    this._aspect = value;
    this.__invalidateProjectionMatrix();
  }

  get fov() {
    return this._fov;
  }

  set fov(value) {
    this._fov = value;
    this.__invalidateProjectionMatrix();
  }

  get near() {
    return this._near;
  }

  set near(value) {
    this._near = value;
    this.__invalidateProjectionMatrix();
  }

  get far() {
    return this._far;
  }

  set far(value) {
    this._far = value;
    this.__invalidateProjectionMatrix();
  }

  protected __updateViewportMatrix() {
    this._aspect = this.width / this.height;
  }

  protected __updateProjectionMatrix(): void {
    mat4.perspective(
      this.__projection,
      this._fov * constant.RAD,
      this._aspect,
      this._near,
      this._far,
    );
  }
}
