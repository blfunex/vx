import {
  mat3,
  mat4,
  ReadonlyMat4,
  ReadonlyVec2,
  ReadonlyVec3,
  vec2,
  vec3,
} from "gl-matrix";
import { gl } from "../gl/canvas";
import Shader from "../gl/Shader";
import { clamp, constant, repeat } from "../math/utils";

const ORIGIN = vec3.fromValues(0, 0, 0);
const UP = vec3.fromValues(0, 1, 0);

const FRONT = vec3.fromValues(0, 0, -1);
const RIGHT = vec3.fromValues(1, 0, 0);

const RIGHT_2D = vec3.create();
const FRONT_2D = vec3.create();

const NORMAL = mat3.create();

export type CameraRequiredUniforms = {
  u_model: string;
  u_viewPosition: string;
  u_viewProjection: string;
  u_normalTransform: string;
};

const DEFAULT_POSITION: vec3 = [0, 0, 1];

const enum CameraInvalidFlags {
  NONE,
  TARGET = 1,
  VIEW = 2,
  VIEWPORT = 4,
  PROJECTION = 8,
  CAMERA = 16,
  ALL = TARGET | VIEW | VIEWPORT | PROJECTION | CAMERA,
}

const enum cam {
  PIo2 = constant.PI / 2,
  ROTATION_LIMIT = PIo2 - 0.000001,
}

export default abstract class Camera {
  private _flags = CameraInvalidFlags.ALL;

  private readonly _position: vec3;
  private readonly _front = vec3.clone(FRONT);
  private readonly _right = vec3.clone(RIGHT);

  constructor(position = DEFAULT_POSITION) {
    this._position = vec3.clone(position);
  }
  private readonly _target: vec3 = [0, 0, 0];
  private readonly _rotation: vec2 = [0, 0];
  private readonly _size: vec2 = [1, 1];

  get front2D(): ReadonlyVec3 {
    const [x, , z] = this._front;
    vec3.set(FRONT_2D, x, 0, z);
    vec3.normalize(FRONT_2D, FRONT_2D);
    return FRONT_2D;
  }

  get right2D(): ReadonlyVec3 {
    const [x, , z] = this._right;
    vec3.set(RIGHT_2D, x, 0, z);
    vec3.normalize(RIGHT_2D, RIGHT_2D);
    return RIGHT_2D;
  }

  get width() {
    return this._size[0];
  }

  get height() {
    return this._size[1];
  }

  get position(): ReadonlyVec3 {
    return this._position;
  }

  get target(): ReadonlyVec3 {
    this._doUpdateTarget();
    return this._target;
  }

  get rotation(): ReadonlyVec2 {
    return this._rotation;
  }

  get front(): ReadonlyVec3 {
    return this._front;
  }

  get right(): ReadonlyVec3 {
    return this._right;
  }

  get size(): ReadonlyVec2 {
    return this._size;
  }

  set width(value) {
    this.resize(value, this.height);
  }

  set height(value) {
    this.resize(this.width, value);
  }

  set size(value: ReadonlyVec2) {
    vec2.copy(this._size, value);
  }

  set position(value: ReadonlyVec3) {
    vec3.copy(this._position, value);
    this.__invalidateTargetVector();
  }

  set target(target: ReadonlyVec3) {
    vec3.copy(this._target, target);

    const front = this._front;
    const right = this._right;

    vec3.sub(front, target, this._position);
    vec3.normalize(front, front);
    vec3.cross(right, front, UP);

    {
      const [fx, fy, fz] = front;

      const rX = Math.atan2(fy, fx) - constant.PI;
      const rY = Math.atan2(fz, fx) + cam.PIo2;

      console.log("Rotation: (%f, %f)", rX * constant.DEG, rY * constant.DEG);
      console.log("Front: (%f, %f, %f)", ...front);
      console.log("Target: (%f, %f, %f)", ...target);
      console.log("Right: (%f, %f, %f)", ...right);
      console.log("Position: (%f, %f, %f)", ...this._position);

      vec2.set(this._rotation, rX, rY);
      this._rotate(0, constant.TAU);
    }

    this.__invalidateViewMatrix();
  }

  lookAt(x: number, y: number, z: number) {
    // console.log("Looking at (%f, %f, %f)", x, y, z);
    this.target = [x, y, z];
  }

  rotate(dRadX: number, dRadY: number) {
    if (!(dRadX || dRadY)) return;

    // console.log("Rotating camera by (%f,%f)", dRadX * constant.DEG, dRadY * constant.DEG);

    this._rotate(dRadX, dRadY);

    const [rX, rY] = this._rotation;

    const front = this._front;
    vec3.rotateX(front, FRONT, ORIGIN, rX);
    vec3.rotateY(front, front, ORIGIN, rY);

    const right = this._right;
    vec3.cross(right, front, UP);

    this.__invalidateTargetVector();
  }

  private _rotate(dRadX: number, dRadY: number) {
    const rotation = this._rotation;
    let [rX, rY] = rotation;
    rX = clamp(rX + dRadX, -cam.ROTATION_LIMIT, cam.ROTATION_LIMIT);
    rY = repeat(rY + dRadY, constant.TAU);
    vec2.set(rotation, rX, rY);
    console.log("Rotation: (%f, %f)", rX * constant.DEG, rY * constant.DEG);
  }

  move(dX: number, dY: number, dZ: number) {
    if (!(dX || dY || dZ)) return;

    const position = this._position;

    vec3.scaleAndAdd(position, position, this.front2D, -dZ);
    vec3.scaleAndAdd(position, position, this.right2D, dX);

    position[1] += dY;

    // console.log("Moving camera (%f, %f, %f)", dX, dY, dZ);

    this.__invalidateTargetVector();
  }

  moveTo(x: number, y: number, z: number) {
    // console.log("Moving camera to (%f, %f, %f)", x, y, z);
    this.position = [x, y, z];
  }

  resize(width: number, height: number) {
    vec2.set(this._size, width, height);
    this.__invalidateViewport();
  }

  protected readonly __view = mat4.create();
  protected readonly __projection = mat4.create();
  protected readonly __camera = mat4.create();

  private _doUpdateTarget() {
    if (this._flags & CameraInvalidFlags.TARGET) {
      // console.log("Updating target vector");
      this.__updateTargetMatrix();
      this._flags -= CameraInvalidFlags.TARGET;
      this.__invalidateViewMatrix();
    }
  }

  private _doUpdateView() {
    this._doUpdateTarget();
    if (this._flags & CameraInvalidFlags.VIEW) {
      // console.log("Updating view matrix");
      this.__updateViewMatrix();
      this._flags -= CameraInvalidFlags.VIEW;
      this.__invalidateCameraMatrix();
    }
  }

  private _doUpdateViewport() {
    if (this._flags & CameraInvalidFlags.VIEWPORT) {
      // console.log("Updating viewport dependent variables");
      this.__updateViewportMatrix();
      this._flags -= CameraInvalidFlags.VIEWPORT;
      this.__invalidateProjectionMatrix();
    }
  }

  private _doUpdateProjection() {
    this._doUpdateViewport();
    if (this._flags & CameraInvalidFlags.PROJECTION) {
      // console.log("Updating projection matrix");
      this.__updateProjectionMatrix();
      this._flags -= CameraInvalidFlags.PROJECTION;
      this.__invalidateCameraMatrix();
    }
  }

  private _doUpdateCamera() {
    this._doUpdateView();
    this._doUpdateProjection();
    if (this._flags & CameraInvalidFlags.CAMERA) {
      // console.log("Updating camera matrix");
      this.__updateCameraMatrix();
      this.__validateAll();
    }
  }

  protected __updateTargetMatrix() {
    vec3.add(this._target, this._position, this._front);
  }

  protected __updateViewMatrix() {
    mat4.lookAt(this.__view, this._position, this._target, UP);
  }

  protected abstract __updateViewportMatrix(): void;

  protected abstract __updateProjectionMatrix(): void;

  protected __updateCameraMatrix() {
    mat4.multiply(this.__camera, this.__projection, this.__view);
  }

  private __validateAll() {
    this._flags = CameraInvalidFlags.NONE;
  }

  protected __invalidateTargetVector() {
    this._flags |= CameraInvalidFlags.TARGET;
  }

  protected __invalidateViewMatrix() {
    this._flags |= CameraInvalidFlags.VIEW;
  }

  protected __invalidateViewport() {
    this._flags |= CameraInvalidFlags.VIEWPORT;
  }

  protected __invalidateProjectionMatrix() {
    this._flags |= CameraInvalidFlags.PROJECTION;
  }

  protected __invalidateCameraMatrix() {
    this._flags |= CameraInvalidFlags.CAMERA;
  }

  get transform() {
    this._doUpdateCamera();
    return this.__camera;
  }

  use(
    shader: Shader,
    model: ReadonlyMat4 = IDENTITY,
    uniforms: false | Partial<CameraRequiredUniforms> = {},
  ) {
    gl.viewport(0, 0, this.width, this.height);

    shader.use();

    this._doUpdateCamera();

    if (uniforms == false) return;

    const {
      u_model = "u_model",
      u_viewPosition = "u_viewPosition",
      u_viewProjection = "u_viewProjection",
      u_normalTransform = "u_normalTransform",
    } = uniforms;

    shader.setMatrix4(u_model, model);
    shader.setVector3(u_viewPosition, this._position);
    shader.setMatrix4(u_viewProjection, this.__camera);
    shader.setMatrix3(u_normalTransform, mat3.normalFromMat4(NORMAL, model));
  }
}

const IDENTITY = mat4.create();
