const enum KeyState {
  UP,
  DOWN,
  REPEAT,
}

type MutableKeyStates = {
  [K in string]?: KeyState;
};

export type KeyStates = Readonly<MutableKeyStates>;

export class Keyboard {
  private _nextKeys: MutableKeyStates = Object.create(DEFAULT_KEY_STATES);
  private _keys: MutableKeyStates = Object.create(DEFAULT_KEY_STATES);

  private _handler: (e: KeyboardEvent) => void;

  get keys(): KeyStates {
    return this._keys;
  }

  constructor() {
    this._handler = e => {
      e.preventDefault();

      const keys = this._nextKeys;

      const key = toKey(e.key);

      // console.log(key);

      switch (e.type) {
        case "keydown":
          keys[key] = e.repeat ? KeyState.REPEAT : KeyState.DOWN;
          return;
        case "keyup":
          keys[key] = KeyState.UP;
          return;
      }
    };

    this._handle(document.addEventListener);
  }

  dispose() {
    this._handle(document.removeEventListener);
  }

  update() {
    const next = this._nextKeys;
    const keys = this._keys;

    for (const k in next) {
      keys[k] = next[k];
    }
  }

  private _handle(method: Function) {
    const handler = this._handler;
    for (const name of KEYBOARD_EVENT_NAMES) {
      method.call(document, name, handler);
    }
  }
}

const KEY_REMAPPINGS: Record<string, string> = {
  " ": "SPACE",
  "ArrowLeft": "LEFT",
  "ArrowRight": "RIGHT",
  "ArrowUp": "UP",
  "ArrowDown": "DOWN",
};

function toKey(k: string) {
  return KEY_REMAPPINGS[k] ?? k.toUpperCase();
}

const KEYBOARD_EVENT_NAMES = ["keydown", "keyup"] as const;

const DEFAULT_KEYS = [
  "SHIFT",
  "CONTROL",
  "ESCAPE",
  "SPACE",
  "LEFT",
  "RIGHT",
  "UP",
  "DOWN",
];
const DEFAULT_KEY_STATES = Object.create(null) as MutableKeyStates;

for (const k of DEFAULT_KEYS) {
  DEFAULT_KEY_STATES[k] = KeyState.UP;
}

for (let c = 65; c <= 90; c++) {
  DEFAULT_KEY_STATES[String.fromCharCode(c)] = KeyState.UP;
}
