export function clamp(x: number, min: number, max: number) {
  return Math.min(Math.max(x, min), max);
}

export function repeat(t: number, length: number) {
  return clamp(t - Math.floor(t / length) * length, 0.0, length);
}

export function mod(n: number, m: number) {
  return ((n % m) + m) % m;
}

export const enum constant {
  PI = 3.141592653589793,
  TAU = PI * 2,
  RAD = PI / 180,
  DEG = 180 / PI,
}
