import { gl } from "./gl/canvas";
import { TileSideMask } from "./mesher/TileSide";
import PerspectiveCamera from "./engine/PerspectiveCamera";
import World from "./world/World";
import Input from "./engine/Input";
import SpectatorController from "./engine/controllers/SpectatorController";

import texturedShader from "./texturedShader";
import VertexObject from "./gl/VertexObject";
import debug from "./utils/debug";
import { ChunkSizes } from "./world/Chunk";

const world = new World();

const dirt = world.addTileController("vx:dirt", {
  usesOcclusionMask: true,
  contributesToChunkHeight: true,
  generate(generator, x, y, z, side) {
    generator.addQuad(side, x, y, z);
  },
  getOccludingMask() {
    return TileSideMask.ALL;
  },
});

for (let x = 0; x < ChunkSizes.LENGTH; x++) {
  for (let z = 0; z < ChunkSizes.LENGTH; z++) {
    for (let y = 0; y < x + z; y++) {
      world.setTile(x, y, z, dirt);
    }
  }
}

const chunk = world.getChunkMaybe(0, 0);

if (!chunk) throw new Error("Expected a chunk at [0,0]");

world.generateMeshes();

const chunk_object = new VertexObject();

chunk_object.add(3, 3, 2).define();

const chunk_mesh = chunk.mesh;

chunk_object.elements.setData(chunk_mesh.elements);
chunk_object.vertices[0].setData(chunk_mesh.vertices);

gl.enable(gl.CULL_FACE);
gl.enable(gl.DEPTH_TEST);
gl.clearColor(0.0, 0.2, 0.4, 1.0);

const input = new Input();
const camera = (debug.camera = new PerspectiveCamera());
const spectator = new SpectatorController(camera, input);

camera.position = [0, 0, 1];
camera.target = [0, 0, 0];

function update() {
  input.update();
  spectator.update();
}

(window.onresize = () => {
  camera.resize(gl.drawingBufferWidth, gl.drawingBufferHeight);
})();

(function render() {
  update();

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  camera.use(texturedShader);

  chunk_object.bind();
  chunk_object.draw(gl.TRIANGLES);

  // debug.point(0, 0, 0);

  requestAnimationFrame(render);
})();
