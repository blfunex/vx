import ImageTexture from "./gl/ImageTexture";
import Shader from "./gl/Shader";

function fetchImageBitmap(source: string) {
  return fetch(source).then(toResponseBlob).then(createImageBitmap);
}

function toResponseBlob(response: Response) {
  return response.blob();
}

const texture = new ImageTexture();

texture.image = await fetchImageBitmap("/assets/sprites/dirt.png");

const shader = new Shader(
  `#version 300 es
  layout(location = 0) in vec4 a_position;
  layout(location = 1) in vec3 a_normal;
  layout(location = 2) in vec2 a_texCoord;

out vec3 v_normal;
out vec2 v_texCoord;
out vec3 v_position;

uniform mat4 u_model;
uniform mat4 u_viewProjection;
uniform mat3 u_normalTransform;

void main() {
  vec4 worldPosition = u_model * a_position;
  gl_Position = u_viewProjection * worldPosition;
  v_normal = u_normalTransform * a_normal;
  v_texCoord = a_texCoord;
  v_position = worldPosition.xyz;
}`,
  `#version 300 es
precision highp float;

in vec2 v_texCoord;
in vec3 v_normal;
in vec3 v_position;

out vec4 o_color;

uniform sampler2D u_texture;
uniform vec3 u_viewPosition;

void main() {
  vec4 albedo = texture(u_texture, v_texCoord);

  vec3 lightPosition = vec3(16.0, 16.0, 16.0);
  vec3 lightColor = vec3(1.0, 1.0, 1.0);

  float ambientStrength = 0.2;
  float specularStrength = 0.5;
  float diffuseStrength = 0.8;

  vec3 N = normalize(v_normal);
  vec3 L = normalize(v_position - lightPosition);
  vec3 V = normalize(v_position - u_viewPosition);

  vec3 A = ambientStrength * lightColor;
  vec3 D = diffuseStrength * lightColor * max(dot(N, L), 0.0);
  vec3 S = specularStrength * lightColor * pow(max(dot(V, L), 0.0), 32.0);

  vec3 color = (A + D + S) * albedo.rgb;

  o_color = vec4(color, albedo.a);
}`,
);

shader.use();
shader.setTexture("u_texture", texture);

export default shader;
