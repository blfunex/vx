export const enum TileSide {
  BOTTOM,
  SOUTH,
  EAST,
  NORTH,
  WEST,
  TOP,
}

export const enum TileSideMask {
  NONE,
  TOP = 1 << TileSide.TOP,
  BOTTOM = 1 << TileSide.BOTTOM,
  NORTH = 1 << TileSide.NORTH,
  SOUTH = 1 << TileSide.SOUTH,
  EAST = 1 << TileSide.EAST,
  WEST = 1 << TileSide.WEST,
  SIDES = NORTH | SOUTH | EAST | WEST,
  ALL = TOP | BOTTOM | SIDES,
}

export function getTileOtherSide(side: TileSide) {
  switch (side) {
    case TileSide.TOP:
      return TileSide.BOTTOM;
    case TileSide.BOTTOM:
      return TileSide.TOP;
    case TileSide.NORTH:
      return TileSide.SOUTH;
    case TileSide.SOUTH:
      return TileSide.NORTH;
    case TileSide.EAST:
      return TileSide.WEST;
    case TileSide.WEST:
      return TileSide.EAST;
  }
}

export function getTileSideMaskString(value: TileSideMask) {
  if (value == TileSideMask.NONE) return "NONE";
  if (value == TileSideMask.ALL) return "ALL";
  if ((value & TileSideMask.SIDES) == TileSideMask.SIDES) {
    let result = "SIDES";
    if (value & TileSideMask.TOP) result += " | TOP";
    if (value & TileSideMask.BOTTOM) result += " | BOTTOM";
    return result;
  }
  const output: string[] = [];
  for (let side = 0; side < 6; side++) {
    // @ts-expect-error // const enums
    if (value & (1 << side)) output.push(TileSide[side]);
  }
  return output.join(" | ");
}
