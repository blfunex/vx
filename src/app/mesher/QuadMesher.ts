import type { ReadonlyVec2, ReadonlyVec3, ReadonlyVec4 } from "gl-matrix";
import type Mesh from "./Mesh";

import { TileSide } from "./TileSide";

export class QuadMesher {
  private quads = createChunkQuadsBuffer();

  private _add(
    side: TileSide,
    depth: number,
    s: number,
    t: number,
    slice?: ReadonlyVec4,
  ) {
    addChunkQuad(this.quads[side], depth, s, t, slice ? this._getSliceId(slice) : -1);
  }

  add(side: TileSide, x: number, y: number, z: number, slice?: ReadonlyVec4) {
    this._add(side, getDepth(side, x, y, z), getS(side, x, z), getT(side, y, z), slice);
  }

  private _slices: ReadonlyVec4[] = [];

  private _sliceIndices = new WeakMap<ReadonlyVec4, number>();

  private _getSliceId(slice: ReadonlyVec4) {
    const map = this._sliceIndices;
    if (map.has(slice)) return map.get(slice)!;
    const id = this._slices.length;
    map.set(slice, id);
    this._slices[id] = slice;
    return id;
  }

  generate(mesh: Mesh, x: number, y: number, z: number) {
    const slices = this._slices;
    for (let side = 0; side < 6; side++) {
      const quads = this.quads[side];
      mesh.index = generateChunkQuads(
        side,
        x,
        y,
        z,
        mesh.index,
        quads,
        mesh.elements,
        mesh.vertices,
        slices,
      );
      quads.length = 0;
    }
    const map = this._sliceIndices;
    for (const slice of slices) {
      map.delete(slice);
    }
    slices.length = 0;
  }
}

function getDepth(side: TileSide, x: number, y: number, z: number) {
  switch (side) {
    case TileSide.EAST:
    case TileSide.WEST:
      return x;
    case TileSide.BOTTOM:
    case TileSide.TOP:
      return y;
    case TileSide.NORTH:
    case TileSide.SOUTH:
      return z;
  }
}

function getS(side: TileSide, x: number, z: number) {
  switch (side) {
    case TileSide.EAST:
    case TileSide.WEST:
      return z;
    case TileSide.SOUTH:
    case TileSide.NORTH:
    case TileSide.TOP:
    case TileSide.BOTTOM:
      return x;
  }
}

function getT(side: TileSide, y: number, z: number) {
  switch (side) {
    case TileSide.TOP:
    case TileSide.BOTTOM:
      return z;
    case TileSide.SOUTH:
    case TileSide.NORTH:
    case TileSide.EAST:
    case TileSide.WEST:
      return y;
  }
}

type Tuple4<T> = readonly [T, T, T, T];

const quadTexCoords: Tuple4<ReadonlyVec2> = [
  [0, 0],
  [0, 1],
  [1, 0],
  [1, 1],
];

/** @internal */
export const sideNormals: ReadonlyVec3[] = [];
const sidePositionOffsets: Tuple4<ReadonlyVec3>[] = [];

const quadIndexOffsets = [0, 1, 2, 2, 1, 3];
const flippedQuadIndexOffsets = [0, 2, 1, 1, 2, 3];

const faceIndexOffsets: number[][] = [];

faceIndexOffsets[TileSide.TOP] = flippedQuadIndexOffsets;

sideNormals[TileSide.TOP] = [0, -1, 0];

sidePositionOffsets[TileSide.TOP] = [
  [-0.5, +0.5, +0.5],
  [-0.5, +0.5, -0.5],
  [+0.5, +0.5, +0.5],
  [+0.5, +0.5, -0.5],
];

faceIndexOffsets[TileSide.BOTTOM] = quadIndexOffsets;

faceIndexOffsets[TileSide.BOTTOM] = quadIndexOffsets;

sideNormals[TileSide.BOTTOM] = [0, 1, 0];

sidePositionOffsets[TileSide.BOTTOM] = [
  [-0.5, -0.5, +0.5],
  [-0.5, -0.5, -0.5],
  [+0.5, -0.5, +0.5],
  [+0.5, -0.5, -0.5],
];

faceIndexOffsets[TileSide.NORTH] = flippedQuadIndexOffsets;

sideNormals[TileSide.NORTH] = [0, 0, 1];

sidePositionOffsets[TileSide.NORTH] = [
  [-0.5, +0.5, -0.5],
  [-0.5, -0.5, -0.5],
  [+0.5, +0.5, -0.5],
  [+0.5, -0.5, -0.5],
];

faceIndexOffsets[TileSide.SOUTH] = quadIndexOffsets;

sideNormals[TileSide.SOUTH] = [0, 0, -1];

sidePositionOffsets[TileSide.SOUTH] = [
  [-0.5, +0.5, +0.5],
  [-0.5, -0.5, +0.5],
  [+0.5, +0.5, +0.5],
  [+0.5, -0.5, +0.5],
];

faceIndexOffsets[TileSide.EAST] = quadIndexOffsets;

sideNormals[TileSide.EAST] = [-1, 0, 0];

sidePositionOffsets[TileSide.EAST] = [
  [+0.5, -0.5, +0.5],
  [+0.5, -0.5, -0.5],
  [+0.5, +0.5, +0.5],
  [+0.5, +0.5, -0.5],
];

faceIndexOffsets[TileSide.WEST] = flippedQuadIndexOffsets;

sideNormals[TileSide.WEST] = [1, 0, 0];

sidePositionOffsets[TileSide.WEST] = [
  [-0.5, -0.5, +0.5],
  [-0.5, -0.5, -0.5],
  [-0.5, +0.5, +0.5],
  [-0.5, +0.5, -0.5],
];

const defaultTextureSlice: ReadonlyVec4 = [0, 0, 1, 1];

function generateQuad(
  side: TileSide,
  x: number,
  y: number,
  z: number,
  index: number,
  elements: number[],
  vertices: number[],
  slice = defaultTextureSlice,
) {
  const [nX, nY, nZ] = sideNormals[side];
  const quadPositionOffsets = sidePositionOffsets[side];

  for (const offset of faceIndexOffsets[side]) {
    elements.push(offset + index);
  }

  const [sX, sY, sW, sH] = slice;

  for (let i = 0; i < 4; i++) {
    const [oX, oY, oZ] = quadPositionOffsets[i];
    const [uvT, uvS] = quadTexCoords[i];

    vertices.push(x + oX, y + oY, z + oZ, nX, nY, nZ, uvT * sW + sX, uvS * sH + sY);
  }

  return index + 4;
}

function addChunkQuad(
  quads: number[],
  depth: number,
  x: number,
  y: number,
  slice: number,
) {
  quads.push(depth, x, y, slice);
}

const sidePositionStepS: ReadonlyVec3[] = [];
const sidePositionStepT: ReadonlyVec3[] = [];

sidePositionStepS[TileSide.SOUTH] =
  sidePositionStepS[TileSide.NORTH] =
  sidePositionStepS[TileSide.BOTTOM] =
  sidePositionStepS[TileSide.TOP] =
    [1, 0, 0];

sidePositionStepT[TileSide.WEST] =
  sidePositionStepT[TileSide.EAST] =
  sidePositionStepT[TileSide.SOUTH] =
  sidePositionStepT[TileSide.NORTH] =
    [0, 1, 0];

sidePositionStepS[TileSide.EAST] =
  sidePositionStepS[TileSide.WEST] =
  sidePositionStepT[TileSide.BOTTOM] =
  sidePositionStepT[TileSide.TOP] =
    [0, 0, 1];

const sideNormalIncrement = sideNormals.map(([x, y, z]) => {
  return [Math.abs(x), Math.abs(y), Math.abs(z)];
});

function generateChunkQuads(
  side: TileSide,
  x: number,
  y: number,
  z: number,
  index: number,
  quads: number[],
  elements: number[],
  vertices: number[],
  slices: readonly ReadonlyVec4[] = [],
) {
  const [nX, nY, nZ] = sideNormalIncrement[side];

  const [ssX, ssY, ssZ] = sidePositionStepS[side];
  const [stX, stY, stZ] = sidePositionStepT[side];

  for (let i = 0; i < quads.length; i += 4) {
    const depth = quads[i];
    const s = quads[i + 1];
    const t = quads[i + 2];

    const slice = quads[i + 3];

    index = generateQuad(
      side,
      x + nX * depth + s * ssX + t * stX,
      y + nY * depth + s * ssY + t * stY,
      z + nZ * depth + s * ssZ + t * stZ,
      index,
      elements,
      vertices,
      slice < 0 ? undefined : slices[slice],
    );
  }

  return index;
}

function createChunkQuadsBuffer(): number[][] {
  return Array.from({ length: 6 }, _ => []);
}
