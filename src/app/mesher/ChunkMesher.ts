import type Mesh from "./Mesh";
import type Chunk from "../world/Chunk";

import { QuadMesher, sideNormals } from "./QuadMesher";
import { getTileOtherSide, TileSide } from "./TileSide";
import {
  ChunkBitwise,
  checkIsOutsideChunk,
  checkIsOutsideWorld,
  ChunkSizes,
} from "../world/Chunk";
import { ReservedTileIDs } from "../world/TileController";

export default class ChunkMesher {
  private quads = new QuadMesher();

  addQuad(side: TileSide, x: number, y: number, z: number) {
    this.quads.add(side, x, y, z);
  }

  private iterate(chunk: Chunk) {
    for (let y = 0; y < chunk.height; y++) {
      for (let z = 0; z < ChunkSizes.LENGTH; z++) {
        for (let x = 0; x < ChunkSizes.LENGTH; x++) {
          const id =
            (chunk.getDataUnchecked(x, y, z) & ChunkBitwise.ID_MASK) >>>
            ChunkBitwise.ID_OFFSET;
          if (id == ReservedTileIDs.AIR || id == ReservedTileIDs.VOID) continue;
          const tile = chunk.world.geTileController(id);
          if (!tile.usesOcclusionMask) {
            tile.generate(this, x, y, z, -1, chunk.lastData);
          }
          const occluded = createOccludedMask(chunk, x, y, z);
          for (let side = 0; side < 6; side++) {
            if (occluded & (1 << side)) continue;
            tile.generate(this, x, y, z, side, chunk.lastData);
          }
        }
      }
    }
  }

  generate(chunk: Chunk) {
    this.iterate(chunk);
    this.quads.generate(
      chunk.mesh,
      chunk.cx * ChunkSizes.LENGTH,
      0,
      chunk.cy * ChunkSizes.LENGTH,
    );
  }

  static generateChunkBorder(mesh: Mesh) {
    const { vertices, elements } = mesh;

    const enum bits {
      X = 1,
      Y = 2,
      Z = 4,
    }

    function value(value: number, bit: bits) {
      return (value & bit) == bit ? (bit == bits.Y ? 256.5 : 16.5) : -0.5;
    }

    for (let i = 0; i < 8; i++) {
      vertices.push(value(i, bits.X), value(i, bits.Y), value(i, bits.Z));
    }

    elements.push(0, 1, 0, 2, 1, 3, 2, 3);
    elements.push(4, 5, 4, 6, 5, 7, 6, 7);
    elements.push(0, 4, 1, 5, 2, 6, 3, 7);

    mesh.index += 12;
  }
}

function createOccludedMask(chunk: Chunk, x: number, y: number, z: number) {
  if (checkIsOutsideWorld(y) || checkIsOutsideChunk(x, z))
    throw new Error(`Can not create occlusion mask outside chunk (${x},${y},${z})`);
  const id = chunk.getDataUnchecked(x, y, z);
  if (id == ReservedTileIDs.AIR || id == ReservedTileIDs.VOID) return 0;
  if (!chunk.world.geTileController(id).usesOcclusionMask) return 0;
  let occluded = 0;
  for (let side = 0; side < 6; side++) {
    const [nx, ny, nz] = sideNormals[side];
    const occlusion = chunk.getOccludingMask(nx + x, ny + y, nz + z);
    const flag = 1 << getTileOtherSide(side);
    occluded |= flag & occlusion;
  }
  return occluded;
}
