export default class Mesh {
  readonly elements: number[] = [];
  readonly vertices: number[] = [];

  index = 0;

  clear() {
    this.index = this.elements.length = this.vertices.length = 0;
  }
}
