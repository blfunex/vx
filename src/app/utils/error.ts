export const enum ErrorCode {
  CREATE_GL_CONTEXT_FAIL,
}

export enum ErrorMessage {
  "Failed to create GL context" = ErrorCode.CREATE_GL_CONTEXT_FAIL,
}

export function error(code: ErrorCode, ...args: unknown[]): never {
  if (process.env.NODE_ENV === "development") {
    console.error(_sprint(ErrorMessage[code], args));
    debugger;
  }
  throw new Error(_sprint(ErrorMessage[code], args));
}

function _sprint(message: string, args: unknown[]) {
  return args.reduce(
    (result: string, arg) =>
      result.replace(/%(s|d|f|o)/, (_, type) => _format(type, arg)),
    message,
  );
}
function _format(type: "s" | "d" | "f" | "o", arg: unknown) {
  const replacement = arg as object;
  switch (type) {
    case "s":
      return replacement.toString();
    case "d":
      return (+replacement | 0).toString();
    case "f":
      return (+replacement).toFixed(2);
    case "o":
      return JSON.stringify(replacement, (k, object) => {
        const value = object[k];

        const isRegExp = value && value instanceof RegExp;
        const isDate = value && value instanceof Date;
        const isFunction = typeof value === "function";

        if (isFunction) {
          return `[λ ${value.name || `.${k}`}]`;
        }

        if (isRegExp) {
          return value.toString();
        }

        if (isDate) {
          return value.getTime();
        }

        return value;
      });
  }
}
