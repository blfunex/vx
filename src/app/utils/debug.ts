import { mat4, ReadonlyVec3, vec3 } from "gl-matrix";
import Camera from "../engine/Camera";
import { gl } from "../gl/canvas";
import Shader from "../gl/Shader";
import VertexObject from "../gl/VertexObject";
import ChunkMesher from "../mesher/ChunkMesher";
import Mesh from "../mesher/Mesh";
import Chunk from "../world/Chunk";

const colored = new Shader(
  `#version 300 es
layout(location = 0) in vec4 a_position;
uniform mat4 u_model;
uniform mat4 u_viewProjection;
uniform float u_size;
void main() {
  gl_PointSize = u_size;
  gl_Position = u_viewProjection * u_model * a_position;
}`,
  `#version 300 es
precision highp float;
uniform vec3 u_color;
out vec4 o_color;
void main() {
  o_color = vec4(u_color, 1);
}`,
);

// const colorful = new Shader(
//   `#version 300 es
// layout(location = 0) in vec4 a_position;
// layout(location = 1) in vec3 a_color;
// uniform mat4 u_model;
// uniform mat4 u_viewProjection;
// uniform float u_size;
// out vec3 v_color;
// void main() {
//   v_color = a_color;
//   gl_PointSize = u_size;
//   gl_Position = u_viewProjection * u_model * a_position;
// }`,
//   `#version 300 es
// precision highp float;
// in vec3 v_color;
// out vec4 o_color;
// void main() {
//   o_color = vec4(v_color, 1);
// }`,
// );

const single_color = new VertexObject();

single_color.add(3).define();

const p = new Float32Array(3);
const pe = new Uint8Array([0]);

const l = new Float32Array(6);
const le = new Uint8Array([0, 1]);

const s = new Float32Array(l, 0, 3);
const d = new Float32Array(l, 12, 3);

const chm = new Mesh();
ChunkMesher.generateChunkBorder(chm);
const ch = new Float32Array(chm.vertices);
const che = new Uint8Array(chm.elements);

const m = mat4.create();
const v3 = vec3.create();

const chc = vec3.fromValues(1, 1, 0);
const lc = vec3.fromValues(1, 1, 1);
const pc = vec3.fromValues(1, 0, 0);

let c: Camera;

function point(
  x: number,
  y: number,
  z: number,
  { camera = c, color = pc as ReadonlyVec3, size = 4 } = {},
) {
  (c = camera).use(colored);

  colored.setVector3("u_color", color);
  colored.setFloat("u_size", size);

  vec3.set(p, x, y, z);

  single_color.bind();
  single_color.vertices[0].setData(p);
  single_color.elements.setData(pe);

  single_color.draw(gl.POINTS);
}

function line(
  sx: number,
  sy: number,
  sz: number,
  ex: number,
  ey: number,
  ez: number,
  { camera = c, color = lc as ReadonlyVec3 },
) {
  (c = camera).use(colored);

  colored.setVector3("u_color", color);

  if (color != lc) {
    vec3.copy(lc, color);
  }

  single_color.bind();

  vec3.set(s, sx, sy, sz);
  vec3.set(d, ex, ey, ez);

  single_color.vertices[0].setData(l);
  single_color.elements.setData(le);

  single_color.draw(gl.LINES);
}

function chunk(chunk: Chunk, { camera = c, color = chc as ReadonlyVec3 } = {}) {
  vec3.set(v3, chunk.cx * 16, 0, chunk.cy * 16);
  mat4.fromTranslation(m, v3);

  (c = camera).use(colored, m);

  colored.setVector3("u_color", color);

  if (color != chc) {
    vec3.copy(chc, color);
  }

  single_color.bind();
  single_color.vertices[0].setData(ch);
  single_color.elements.setData(che);

  single_color.draw(gl.LINES);
}

const debug = {
  get camera() {
    return c;
  },
  set camera(value: Camera) {
    c = value;
  },
  point,
  line,
  chunk,
};

export default debug;
